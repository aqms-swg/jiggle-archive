# jiggle-archive

This is a holding place for previously built Jiggle packages that are still in use, 
but for which the source is not kept in git.

All the jiggle jar files in this repository work with Oracle databases only. 
There are two versions, one that works with AQMS databases that have the GTYPE schema, and one that does not.
The jiggle jars in this repo expect all the supporting libraries to be in the same directory (aka folder) as
the jiggle jar itself.

## jiggle16-client.zip: All in one zip-file
For a quick setup download and unzip into a root directory of your choice:  https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/jiggle16-client.zip

Unzipping creates a directory named "Jiggle" below the root directory, and all 
.jar files and scripts are located in a /bin subdirectory, all properties files 
are located in a /props subdirectory, and all documentation files are located in 
a /doc subdirectory.

After extraction, update the included jiggle.jar by downloading the appropriate 
jiggle.. jar (see links below) and rename the new file to jiggle.jar after deleting 
the old one. You do not need to download any other files.

Included are example startup scripts for Unix (jiggle.command) and Windows (jiggle.bat). 
Edit these files, if necessary, to change java runtime reference, directory path variables, 
or input property filename. Once the application is started using a script, 
a popup dialog will prompt you for your local network code and the database 
connection values if they are undefined in the properties file. Channel data are 
read from a disk cache file, and if it does not already exist, a database query 
will build it; however, the query does take quite a while to process, so be patient.

## GTYPE-enabled AQMS schema, Jiggle version 2017-06-27
* Java 1.8: https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/jiggle18-p7g.jar (requires ojdbc7.jar)
* Java 1.7: https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/jiggle17-p6g.jar
* Java 1.6: https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/jiggle16-p6g.jar

## AQMS schema without GTYPE, Jiggle version 2017-06-27
* Java 1.8: https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/jiggle18-p7.jar (requires ojdbc7.jar)
* Java 1.7: https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/jiggle17-p6.jar
* Java 1.6: https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/jiggle16-p6.jar

## Supporting packages

* These all go in the bin directory that contains the Jiggle jar file:
  * all-in-one14.zip (Java 1.4 compiled) --> needs to be unzipped
  * mapdata.zip --> needs to be unzipped
  * swarm-avo.zip --> needs to be unzipped
  * ojdbc6.jar
  * ojdbc7.jar
* Unzip the properties files into a prop directory:
  * properties.zip

* Example mung properties (ask SCSN how this is used): https://gitlab.com/aqms.swg/jiggle-archive/-/blob/master/mung.zip
